Re-branded KalturaDeviceInfo to provides GSF id (needed for Google uncertified devices) and checks about Widevine, Treble, a/b & media decoders (and all others possible device system definitions).

Basic json example, it shows how much any app, can silently retrieve devices characteristics, but also simply provides :
It collects some diagnostic information about an Android device and allows sharing it using a standard share Intent.

 * testing any device sample, without Dev./Options unlocked, before purchasing (live or via email request to the seller)
 * add-on for static passive reporting in AOSP roms (conversely to active stats)
 * Treble advanced reviewing specifics
 * all-in-one floss without internet leak

JSON report doesn't include any {action}->privateIDs ; feel free to Post everywhere
(*.json can be easily browsed/parsed with FirefoxPC)

 - detection getprop for Treble & A/B seamless update + designation Soc & Kernel version
 - ... SharedLibs, Features, SecurityProviders, /proc/meminfo, GLESversion +...
 - Services & init_svc & {action}-> getprop
 - New Treble checks (ro.vndk.version) & ScreenMetrics
 - improve gps.conf & treble + matrix
 -/vendor detection, mounts, camera & cameraAPi2... (and Matrix_bug)
 - CameraAPi2 global specs
 - Display HDR modes
 - etc/gps.conf miss in "private" dialog-box
 - microG spec(s) detection
 - multi screens support
 - all sensors with mA consumption

code https://bitbucket.org/oF2pks/kaltura-device-info-android/src/master/
prim-origin https://github.com/kaltura/kaltura-device-info-android

nota: works on JellyBean and up ; with Oreo, Android_id is discontinued, and Serial_id deprecated due to <read_phone_state> permission. 