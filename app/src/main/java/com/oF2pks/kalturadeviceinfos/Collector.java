package com.oF2pks.kalturadeviceinfos;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.admin.DevicePolicyManager;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.ConfigurationInfo;
import android.content.pm.FeatureInfo;
import android.content.pm.PackageManager;
import android.drm.DrmInfo;
import android.drm.DrmInfoRequest;
import android.drm.DrmManagerClient;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.display.DisplayManager;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaDrm;
import android.media.UnsupportedSchemeException;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Range;
import android.view.Display;
import android.view.WindowManager;

//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.common.api.Status;
//import com.google.android.gms.safetynet.SafetyNet;
//import com.google.android.gms.safetynet.SafetyNetApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.Provider;
import java.security.Security;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.SortedSet;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.UUID;

import static com.oF2pks.kalturadeviceinfos.Utils.blindM;
import static com.oF2pks.kalturadeviceinfos.Utils.colorFilter;
import static com.oF2pks.kalturadeviceinfos.Utils.errorS;
import static com.oF2pks.kalturadeviceinfos.Utils.fCamList;
import static com.oF2pks.kalturadeviceinfos.Utils.getProp;
import static com.oF2pks.kalturadeviceinfos.Utils.getZinfo;
import static com.oF2pks.kalturadeviceinfos.Utils.iCamList;
import static com.oF2pks.kalturadeviceinfos.Utils.upR;


/**
 * Created by noamt on 17/05/2016.
 */
class Collector {
    private static final String TAG = "Collector";
    static final UUID WIDEVINE_UUID = new UUID(0xEDEF8BA979D64ACEL, 0xA3C827DCD51D21EDL);
    static final UUID CLEARKEY_UUID = new UUID(-0x1d8e62a7567a4c37L, 0x781AB030AF78D30EL);
    static final UUID PLAYREADY_UUID = new UUID(0x9A04F07998404286L, 0xAB92E65BE0885F95L);
    private final Context mContext;
    private final boolean includeSafetyNet;
    private final JSONObject mRoot = new JSONObject();
    private final int numCameras = Camera.getNumberOfCameras();
    private int nbrCamApi2 = 0;

    private static String sReport;
    private native int get_binder_version();

    static String getReport(Context ctx, boolean includeSafetyNet) {
        sReport = null;
        if (sReport == null) {
            Collector collector = new Collector(ctx, includeSafetyNet);
            JSONObject jsonReport = collector.collect();

            try {
                sReport = jsonReport.toString(4);
                sReport = sReport.replace("\\/", "/");
                
            } catch (JSONException e) {
                sReport = "{}";
            }
        }

        return sReport;
    }
    
    Collector(Context context, boolean includeSafetyNet) {
        mContext = context;
        this.includeSafetyNet = includeSafetyNet;
    }
    
    JSONObject collect() {
        final JSONObject[] safetyNetResult = new JSONObject[1];
//        Thread safetyNetThread = new Thread() {
//            @Override
//            public void run() {
//                try {
//                    safetyNetResult[0]=collectSafetyNet();
//                } catch (JSONException e) {
//                    Log.e(TAG, "Failed converting safetyNet response to JSON");
//                }
//            }
//        };
//
//        if (includeSafetyNet) {
//            safetyNetThread.start();
//        }

        try {
            JSONObject root = mRoot;
            root.put("#META=", meta());
            root.put("#ARCH=", systemArch());
            root.put("#DRM=", drmInfo());
            root.put("#ANDROID",androidInfo());
            root.put("#SYSTEM=", systemInfo());
            root.put("#DISPLAY=", displayInfo());
            root.put("#MEDIA=", mediaCodecInfo());
            root.put("#SECURITY=", securityProviders());
            root.put("#LOCATION&microG=", locationMicroG());
            root.put("#CAMERA=",cameraOldAPI());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) root.put("#CAMERA2API=",camera2API());
            root.put("#LIBS&FEATURES=", systemFL());
            root.put("#SERVICES&SVC=", dumpsysL());
            root.put("#ROOT?=", rootInfo());
            if (getProp("ro.treble.enabled").equals("true")) root.put("#TREBLE=", trebleInfo());
            root.put("#MOUNTS", selfMounts());
            root.put("#SENSORS=", sensorInfo());

//            if (includeSafetyNet) {
//                safetyNetThread.join(20 * 1000);
//                root.put("safetyNet", safetyNetResult[0]);
//            }
            
        } catch (JSONException e) {
            Log.e(TAG, "Error");
//        } catch (InterruptedException e) {
//            Log.d(TAG, "Interrupted");
        }
        return mRoot;
    }

    private JSONObject sensorInfo() throws JSONException {
        JSONObject archT = new JSONObject();
        JSONObject archV = new JSONObject();
        JSONObject archY = new JSONObject();
        JSONObject arch = new JSONObject();
        SensorManager sm = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> listSensor=new ArrayList<>(sm.getSensorList(Sensor.TYPE_ALL));
        archT.put("TOTAL", listSensor.size());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) Collections.sort(listSensor, new Comparator<Sensor>() {
            @Override
            public int compare(final Sensor a, final Sensor b) {
                /*if (a.getType() != b.getType()) return (a.getType() - b.getType());
                else*/ return
                        (a.getStringType()+a.getVendor()+a.getName()).compareToIgnoreCase
                        (b.getStringType()+b.getVendor()+b.getName());
            }
        });
        else Collections.sort(listSensor, new Comparator<Sensor>() {
            @Override
            public int compare(final Sensor a, final Sensor b) {
                if (a.getType() != b.getType()) return (a.getType() - b.getType());
                else return ((a.getVendor() + a.getName()).compareToIgnoreCase
                        (b.getVendor() + b.getName()));
            }
        });;

        String vendor0 = "$&£";
        int type0 = 0;
        String typeS0="";
        for (Sensor sensor : listSensor) {
            if (type0 != sensor.getType()) {
                if (archV.length() != 0) {
                    archY.put(vendor0,archV);
                    archV = new JSONObject();
                    archT.put(type0+"_"+typeS0,archY);
                    archY = new JSONObject();
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) typeS0 = sensor.getStringType();
                type0 = sensor.getType();
                if (sensor.getVendor().length() == 0) vendor0 ="Unknow";
                else vendor0 = sensor.getVendor();
            } else if (!vendor0.equals(sensor.getVendor())) {
                if (archV.length() != 0) {
                    archY.put(vendor0,archV);
                    archV = new JSONObject();
                }
                if (sensor.getVendor().length() == 0) vendor0 ="Unknow";
                else vendor0 = sensor.getVendor();
            }
            arch =new JSONObject();
            arch.put("Vendor", sensor.getVendor());
            arch.put("Version", sensor.getVersion());
            if (Build.VERSION.SDK_INT >= 20)
                arch.put("StringType",sensor.getStringType());
            arch.put("Type", sensor.getType());
            arch.put("mA", sensor.getPower());
            if (Build.VERSION.SDK_INT >= 21) {
                arch.put("ReportingMode", sensor.getReportingMode());
                arch.put("isWakeUpSensor", sensor.isWakeUpSensor());
            }
            if (Build.VERSION.SDK_INT >= 24) {
                arch.put("Id", sensor.getId());
                arch.put("isDynamicSensor", sensor.isDynamicSensor());
                arch.put("isAdditionalInfoSupported", sensor.isAdditionalInfoSupported());
            }
            arch.put("Resolution", sensor.getResolution());
            arch.put("MaximumRange", sensor.getMaximumRange());
            arch.put("MinDelay", sensor.getMinDelay());
            if (Build.VERSION.SDK_INT >= 21) {
                arch.put("MaxDelay", sensor.getMaxDelay());
            }
            if (Build.VERSION.SDK_INT >= 26) {
                arch.put("HighestDirectReportRateLevel", sensor.getHighestDirectReportRateLevel());
                arch.put("isDirectChannelTypeSupported", sensor.isDirectChannelTypeSupported(0));
            }
            if (Build.VERSION.SDK_INT >= 19) {
                arch.put("FifoMaxEventCount", sensor.getFifoMaxEventCount());
                arch.put("FifoReservedEventCount", sensor.getFifoReservedEventCount());
            }

            archV.put(sensor.getName()+"_"+sensor.getType(),arch);
        }
        archY.put(vendor0,archV);
        archT.put(type0+"_"+typeS0,archY);
        return archT;
    }

    private JSONObject locationMicroG() throws JSONException {
        JSONObject archT = new JSONObject();
        JSONObject arch = new JSONObject();
        String tmp = "";
        int id;
        id = mContext.getResources().getIdentifier("android:array/config_locationProviderPackageNames",null,null);
        List<ApplicationInfo> apps = mContext.getPackageManager().getInstalledApplications(0);
        if (Build.VERSION.SDK_INT >= 23) {
            WifiManager wf= (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            String[] z = {
                    "Disabling",
                    "Disabled",
                    "Enabling",
                    "Enable",
                    "Unknown"
            };
            if (wf.getWifiState() < z.length)
                arch.put("getWifiState", z[wf.getWifiState()]);
            else arch.put("getWifiState", wf.getWifiState());

            arch.put("isScanAlwaysAvailable", wf.isScanAlwaysAvailable());
            if (Build.VERSION.SDK_INT >= 30) {
                arch.put("getMaxSignalLevel", wf.getMaxSignalLevel());
                arch.put("isAutoWakeupEnabled", wf.isAutoWakeupEnabled());
                arch.put("isScanThrottleEnabled", wf.isScanThrottleEnabled());
            }
            archT.put("wifi",arch);
            arch = new JSONObject();

            ArrayList<String> appsNames= new ArrayList<>();
            UsageStatsManager mUsageStats;
            mUsageStats = mContext.getSystemService(UsageStatsManager.class);
            for (ApplicationInfo app : apps) {
                appsNames.add(app.packageName);
                if (app.packageName.contains(".location")) {
                    arch.put(app.sourceDir, (mUsageStats.isAppInactive(app.packageName) ? "0ff " : " ON "));
                }
            }
            archT.put("detectionLocation",arch);
            arch = new JSONObject();

            if (id!=0){
                String[] lProviders = mContext.getResources().getStringArray(id);
                for (String s: lProviders){
                    if (appsNames.contains(s)) {
                        arch.put(s, (mUsageStats.isAppInactive(s) ? "0ff " : " ON "));
                    } else arch.put(s, "000 ");
                }
            }
            archT.put("providers",arch);

        } else {
            for(ApplicationInfo app : apps) {
                if (app.packageName.contains(".location")) {
                    arch.put(app.sourceDir,"installed");
                }
            }
            archT.put("detectionLocation",arch);
            arch = new JSONObject();

            if (id!=0){
                String[] lProviders = mContext.getResources().getStringArray(id);
                for (String s: lProviders) arch.put(s,"installed");
            }
            archT.put("providers",arch);
        }

        arch = new JSONObject();
        String[] bEnables = mContext.getResources().getStringArray(R.array.bEnables);/*{//TT"config_enableUpdateableTimeZoneRules",
                "config_enableNetworkLocationOverlay",
                "config_enableFusedLocationOverlay",
                "config_enableHardwareFlpOverlay",
                "config_enableGeocoderOverlay",
                "config_enableGeofenceOverlay"
        };*/
        String[] sEnables = mContext.getResources().getStringArray(R.array.sEnables);/*{//TT"config_timeZoneRulesUpdaterPackage",
                "config_networkLocationProviderPackageName",
                "config_fusedLocationProviderPackageName",
                "config_hardwareFlpPackageName",
                "config_geocoderProviderPackageName",
                "config_geofenceProviderPackageName",
                "config_defaultNetworkRecommendationProviderPackage"
        };*/
        boolean b = false;
        for (int i=0; i< bEnables.length; i++) {
            id = mContext.getResources().getIdentifier("android:bool/"+bEnables[i],null,null);
            if (id!=0) {
                b = mContext.getResources().getBoolean(id);
                archT.put(bEnables[i].substring(6), b);
                id = mContext.getResources().getIdentifier("android:string/"+sEnables[i],null,null);
                if (id!=0) {
                    archT.put(sEnables[i].substring(6)
                            , String.format("%-6s",mContext.getResources().getText(id)));
                } else{
                    archT.put(sEnables[i].substring(6)
                            , "value_MISSING!?");
                }
            } else {
                archT.put(bEnables[i].substring(6), "bool_MISSING!?");
                id = mContext.getResources().getIdentifier("android:string/"+sEnables[i],null,null);
                if (id!=0) {
                    archT.put("bool_MISSING!?"+sEnables[i].substring(6)
                            , String.format("%-6s",mContext.getResources().getText(id)));
                }

            }
        }


        id = mContext.getResources().getIdentifier("android:string/config_defaultNetworkRecommendationProviderPackage",null,null);
        if (id!=0) archT.put("_defaultNetworkRecommendationProviderPackage", mContext.getResources().getString(id));//+mContext.getResources().getResourceTypeName(id);

        id = mContext.getResources().getIdentifier("android:string/config_defaultDndAccessPackages",null,null);
        if (id!=0) archT.put("_defaultDndAccessPackages",mContext.getResources().getString(id));//+mContext.getResources().getResourceTypeName(id);

        id = mContext.getResources().getIdentifier("android:string/permlab_fakePackageSignature",null,null);
        if (id!=0) archT.put("microG_permlab_fakePackageSignature",mContext.getResources().getString(id));
        else archT.put("microG_permlab_fakePackageSignature","V0ID");//+mContext.getResources().getResourceTypeName(id);

        tmp=getProp("ro.services.whitelist.packagelist");
        if (tmp.length()>0) archT.put("OMNIROM_whitelistDetected:", tmp);

        return archT;
    }

    private JSONObject trebleInfo() throws JSONException {
        String ldConfig = "/linkerconfig/ld.config.txt";
        String s = getProp("ro.vndk.version");
        JSONObject marshmalow = new JSONObject();
        JSONObject archT = new JSONObject();
        marshmalow.put("TrebleGetprop", getProp("ro.treble.enabled"));
        marshmalow.put("ro.vendor.vndk.version", getProp("ro.vendor.vndk.version"));
        marshmalow.put("ro.vndk.lite", getProp("ro.vndk.lite"));
        marshmalow.put("ro.vndk.version", s);
        marshmalow.put("persist_vndk", getProp("persist.sys.vndk"));

        if (!Utils.upR(s)) {
            if (getProp("ro.vndk.lite").equals("true")) ldConfig = "/system/etc/ld.config.vndk_lite.txt";
            else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) ldConfig = "/system/etc/ld.config"+(s.equals("") ?"": "."+s)+".txt";

            if (!(s.equals(""))) s="-"+s;
            archT.put("system/lib/vndk" + s, getZinfo("ls /system/lib/vndk" + s, "/",false));
            archT.put("system/lib/vndk-sp" + s, getZinfo("ls /system/lib/vndk-sp" + s, "/",false));
            archT.put("vendor/lib/vndk", getZinfo("ls /vendor/lib/vndk", "/",false));
            archT.put("vendor/lib/vndk" + s, getZinfo("ls /vendor/lib/vndk" + s, "/",false));
            archT.put("vendor/lib/vndk-sp" + s, getZinfo("ls /vendor/lib/vndk-sp" + s, "/",false));
            marshmalow.put("lib(s)", archT);

            archT = new JSONObject();
            archT.put("system/lib64/vndk" + s, getZinfo("ls /system/lib64/vndk" + s, "/",false));
            archT.put("system/lib64/vndk-sp" + s, getZinfo("ls /system/lib64/vndk-sp" + s, "/",false));
            archT.put("vendor/lib64/vndk", getZinfo("ls /vendor/lib64/vndk", "/",false));
            archT.put("vendor/lib64/vndk" + s, getZinfo("ls /vendor/lib64/vndk" + s, "/",false));
            archT.put("vendor/lib64/vndk-sp" + s, getZinfo("ls /vendor/lib64/vndk-sp" + s, "/",false));
            marshmalow.put("lib64(s)", archT);
        }

        archT = new JSONObject();
        archT.put("ld_configPath", ldConfig);
        try {
            Process p = Runtime.getRuntime().exec("cat " + ldConfig);
            InputStream is = null;
            boolean bVendor = false;
            //if (p.waitFor() == 0) {
            is = p.getInputStream();
            /*} else {
                is = p.getErrorStream();
            }*/
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String tmp;
            String tmp2 = null;

            JSONObject archV = new JSONObject();
            while ((tmp = br.readLine()) != null) {
                if (bVendor) {
                    //tmp2 += tmp;
                    if (tmp.startsWith("[")) {
                        break;
                    }
                    if (tmp.startsWith("additional.namespaces =")) archV.put("additionnal.namespaces",tmp.substring(tmp.indexOf("=")+2));
                    if (tmp.startsWith("namespace.default.isolated =")) archV.put("namespace.default.isolated",tmp.substring(tmp.indexOf("=")+2));
                    if (tmp.startsWith("namespace.default.visible =")) archV.put("namespace.default.visible",tmp.substring(tmp.indexOf("=")+2));
                    if (tmp.startsWith("namespace.default.links =")) archV.put("namespace.default.links",tmp.substring(tmp.indexOf("=")+2));
                } else if (tmp.startsWith("[vendor]")) bVendor = true;
            }
            is.close();
            br.close();
            if (bVendor) archT.put("[Vendor]",archV);
            else archT.put("[Vendor]","Unknow");
            //archT.put("zz",tmp2);
        } catch (Exception ex) {
            archT.put(ldConfig, errorS(ex));
        }
        marshmalow.put("ld_config", archT);
        return marshmalow;
    }

    private JSONObject cameraOldAPI() throws JSONException {
        JSONObject archT = new JSONObject();
        String sCams = "";
        Camera camera = null;
        Camera.CameraInfo info = new Camera.CameraInfo();
        for (int i = 0; i < numCameras; i++) {
            Camera.getCameraInfo(i, info);
            try {
                camera = Camera.open(i);
                Camera.Parameters parms = camera.getParameters();
                sCams = parms.flatten();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    archT.put(Integer.toString(i) + "/" + numCameras + (info.facing == 0 ? "Back" : "Front"),
                            Utils.semicolonSortedJson(sCams, "=", ";"));
                else archT.put(Integer.toString(i) + "/" + numCameras + (info.facing == 0 ? "Back" : "Front"),
                        Utils.semicolonJson(sCams, "=", ";"));
                camera.release();
            } catch (Exception e) {
                archT.put("MISSING permission_CAMERA","!Marshmallow and up!");
            }
        }
        return archT;
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private JSONObject camera2API() throws JSONException {
    JSONObject archT = new JSONObject();
    CameraManager manager = (CameraManager) mContext.getSystemService(Context.CAMERA_SERVICE);
    if (manager==null) return archT.put("WARNING","CameraAPi2 removed");
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
        try {
            archT.put("getConcurrentCameraIds",manager.getConcurrentCameraIds().toString());
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }
    try {
        for (String cameraId : manager.getCameraIdList()) {
            nbrCamApi2++;
            CameraCharacteristics chars = manager.getCameraCharacteristics(cameraId);
            String s=cameraId;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                s+= manager.getCameraExtensionCharacteristics(cameraId).getSupportedExtensions().toString();
            }
            s+= "=";
            switch (chars.get(CameraCharacteristics.LENS_FACING)){
                case 0:
                    s+="front"; break;
                case 1:
                    s+="back"; break;
                case 2:
                    s+="external";
            }
            s+="<";
            switch (chars.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL)){
                case 0:
                    s+="LIMITED"; break;
                case 1:
                    s+="FULL"; break;
                case 2:
                    s+="LEGACY"; break;
                case 3:
                    s+="LEVEL3"; break;
                case 4:
                    s+="EXTERNAL";
            }
            s+=">";// + colorFilter(chars);
            s+=chars.get(CameraCharacteristics.SENSOR_INFO_PIXEL_ARRAY_SIZE)+"("+chars.get(CameraCharacteristics.SENSOR_INFO_SENSITIVITY_RANGE)+")";
            archT.put(s,tech2API(chars));
            // Do something with the characteristics
        }
    } catch (Exception e){//CameraAccessException
        e.printStackTrace();
    //} catch(NullPointerException e){
        // Currently an NPE is thrown when the Camera2API is used but not supported on the
        // device this code runs.
    }
    return archT.put("nbrCamsAPI2",nbrCamApi2);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)//https://cs.android.com/android/platform/superproject/+/master:system/media/camera/include/system/camera_metadata_tags.h
    private JSONObject tech2API(CameraCharacteristics chars) throws JSONException {
        JSONObject arch = new JSONObject();
        CameraCharacteristics.Key c;
        String tmp ="";
        String oo="";
        boolean logicalCam =false;
        int[] ii=chars.get(CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES);//https://android.googlesource.com/platform/cts/+/master/tests/camera/src/android/hardware/camera2/cts/ExtendedCameraCharacteristicsTest.java#2087
        String[] z={"OFF","ON"};
        String[] zz={"BACKWARD_COMPATIBLE"//21"
                ,"MANUAL_SENSOR"//21"
                ,"MANUAL_POST_PROCESSING"//21"
                ,"RAW"//21"
                ,"PRIVATE_REPROCESSING"//23"
                ,"READ_SENSOR_SETTINGS"//22"
                ,"BURST_CAPTURE"//22"
                ,"YUV_REPROCESSING"//23"
                ,"DEPTH_OUTPUT"//23"
                ,"CONSTRAINED_HIGH_SPEED_VIDEO"//23"
                ,"MOTION_TRACKING"//28"
                ,"LOGICAL_MULTI_CAMERA"//28" =11
                ,"MONOCHROME"//28
                ,"SECURE_IMAGE_DATA"//29
                ,"SYSTEM_CAMERA"//30
                ,"OFFLINE_PROCESSING"//30
                ,"ULTRA_HIGH_RESOLUTION_SENSOR"//31
                ,"REMOSAIC_REPROCESSING"//31
            };//31

        for (int i:ii) {
            tmp+="+ "+(i<zz.length ? zz[i] : i);
            if (i == 11) logicalCam = true;//LOGICAL_MULTI_CAMERA
        }
        arch.put("MAIN_specs",tmp);tmp="";
        arch.put("colorFilter",colorFilter(chars));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            arch.put("api28v", "_"+chars.get(CameraCharacteristics.INFO_VERSION));
            arch.put("api28STATISTICS_INFO_AVAILABLE_OIS_DATA_MODES", iCamList(chars, CameraCharacteristics.STATISTICS_INFO_AVAILABLE_OIS_DATA_MODES, z));
            if (logicalCam) {
                CameraManager manager = (CameraManager) mContext.getSystemService(Context.CAMERA_SERVICE);
                for (String cameraReal : chars.getPhysicalCameraIds()) {
                    tmp += "%"+cameraReal;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) try {
                        arch.put(cameraReal, tech2API(manager.getCameraCharacteristics(cameraReal)));
                    }catch (Exception e){
                    }
                }
                tmp =+ chars.getPhysicalCameraIds().size() +":";
                arch.put("api28PhysicalIds",tmp);tmp="";
                try {arch.put("api28LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE"
                            , chars.get(CameraCharacteristics.LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE) == 0
                                    ? "approximate" : "calibrated");
                } catch (Exception e) {
                    arch.put("api28LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE", errorS(e));
                }
            }
        }

        Range[] jj = chars.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES);
        for (Range r:jj) tmp+=r.toString();
        arch.put("FPS_ranges",tmp);tmp="";

        arch.put("Apertures", fCamList(chars,CameraCharacteristics.LENS_INFO_AVAILABLE_APERTURES));
        arch.put("Focal_lenghts", fCamList(chars,CameraCharacteristics.LENS_INFO_AVAILABLE_FOCAL_LENGTHS));

        arch.put("AE_modes",iCamList(chars, CameraCharacteristics.CONTROL_AE_AVAILABLE_MODES, new String[]{"OFF","ON","ON_AUTO_FLASH","ON_ALWAYS_FLASH","ON_AUTO_FLASH_REDEYE","ON_EXTERNAL_FLASH"}));

        arch.put("AF_modes",iCamList(chars, CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES, new String[]{"OFF","AUTO","MACRO","CONTINUOUS_VIDEO","CONTINUOUS_PICTURE","EDOF"}));

        arch.put("AWB_modes",iCamList(chars, CameraCharacteristics.CONTROL_AWB_AVAILABLE_MODES, new String[]{"OFF","AUTO","INCANDESCENT","FLUORESCENT","WARM_FLUORESCENT","DAYLIGHT","CLOUDY_DAYLIGHT","TWILIGHT","SHADE"}));

        arch.put("Scene_modes",iCamList(chars, CameraCharacteristics.CONTROL_AVAILABLE_SCENE_MODES, new String[]{"DISABLED","FACE_PRIORITY","ACTION","PORTRAIT","LANDSCAPE","NIGHT","NIGHT_PORTRAIT","THEATRE","BEACH","SNOW","SUNSET","STEADYPHOTO","FIREWORKS","SPORTS","PARTY","CANDLELIGHT","BARCODE","HIGH_SPEED_VIDEO","HDR","FACE_PRIORITY_LOW_LIGHT"}));

        arch.put("AE_antibanding",iCamList(chars, CameraCharacteristics.CONTROL_AE_AVAILABLE_ANTIBANDING_MODES, new String[]{"OFF","50HZ","60HZ","AUTO"}));

        arch.put("Color_correction_modes",iCamList(chars, CameraCharacteristics.COLOR_CORRECTION_AVAILABLE_ABERRATION_MODES, new String[]{"OFF","FAST","HIGH_QUALITY"}));

        arch.put("Effects",iCamList(chars, CameraCharacteristics.CONTROL_AVAILABLE_EFFECTS, new String[]{"OFF","MONO","NEGATIVE","SOLARIZE","SEPIA","POSTERIZE","WHITEBOARD","BLACKBOARD","AQUA"}));

        arch.put("Edge",iCamList(chars, CameraCharacteristics.EDGE_AVAILABLE_EDGE_MODES, new String[]{"OFF","FAST","HIGH_QUALITY","ZERO_SHUTTER_LAG"}));

        arch.put("Noise",iCamList(chars, CameraCharacteristics.NOISE_REDUCTION_AVAILABLE_NOISE_REDUCTION_MODES, new String[]{"OFF","FAST","HIGH_QUALITY","MINIMAL","ZERO_SHUTTER_LAG"}));

        arch.put("Sensor_test_pattern",iCamList(chars, CameraCharacteristics.SENSOR_AVAILABLE_TEST_PATTERN_MODES, new String[]{"OFF","SOLID_COLOR","COLOR_BARS","COLOR_BARS_FADE_TO_GRAY","PN9","CUSTOM1"}));//err CUSTOM1=256 !!

        arch.put("Hot_pixel",iCamList(chars, CameraCharacteristics.HOT_PIXEL_AVAILABLE_HOT_PIXEL_MODES,new String[]{"OFF","FAST","HIGH_QUALITY"}));

        arch.put("OIS_modes",iCamList(chars, CameraCharacteristics.LENS_INFO_AVAILABLE_OPTICAL_STABILIZATION, new String[]{"OFF","ON"}));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // CONTROL_AE_LOCK_AVAILABLE CONTROL_AWB_LOCK_AVAILABLE DEPTH_DEPTH_IS_EXCLUSIVE
            arch.put("Controls",iCamList(chars, CameraCharacteristics.CONTROL_AVAILABLE_MODES, new String[]{"OFF","AUTO","USE_SCENE_MODE","OFF_KEEP_STATE","USE_EXTENDED_SCENE_MODE"}));
        }

        //24 CONTROL_POST_RAW_SENSITIVITY_BOOST_RANGE

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            arch.put("Distorsion_correction",iCamList(chars, CameraCharacteristics.DISTORTION_CORRECTION_AVAILABLE_MODES,new String[]{"OFF","FAST","HIGH_QUALITY"}));
            arch.put("infoVersion", chars.get(CameraCharacteristics.INFO_VERSION));
            // LENS_DISTORTION
        }

        //30 CONTROL_AVAILABLE_EXTENDED_SCENE_MODE_CAPABILITIES CONTROL_ZOOM_RATIO_RANGE

        List<CameraCharacteristics.Key<?>> tt = chars.getKeys();
        ListIterator<CameraCharacteristics.Key<?>> it = tt.listIterator();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            String s = "";
            List<CameraCharacteristics.Key<?>> ttPermRequired = chars.getKeysNeedingPermission();
            arch.put("sizeKeysNeedingPermission" , chars.getKeysNeedingPermission().size());
            while(it.hasNext()){
                c=it.next();
                tmp = chars.get(c).toString();
                s= ttPermRequired.contains(c) ? "*" : "";//permission.CAMERA https://developer.android.com/reference/android/hardware/camera2/CameraCharacteristics.html#getKeysNeedingPermission()
                if (!tmp.startsWith("[") || tmp.endsWith("]")) {
                    arch.put(c.getName(), s + tmp);
                } else {
                    oo+="/" +s + c.getName();
                    //arch.put(c.getName(),c.getClass().getSimpleName());
                }
            }
        } else {
            while(it.hasNext()) {
                c = it.next();
                tmp = chars.get(c).toString();
                if (!tmp.startsWith("[") || tmp.endsWith("]")) arch.put(c.getName(), tmp);
                else {
                    oo += "/" + c.getName();
                    //arch.put(c.getName(),c.getClass().getSimpleName());
                }
            }
        }
        oo=oo.replaceAll("\\."," ");
        if (oo.length()>0)arch.put("UNSOLVEDs",oo);
        return arch;

    }

    private JSONObject selfMounts() throws JSONException {
        JSONObject archT = new JSONObject();
        JSONObject arch = new JSONObject();
        String[] s3 =new String[3];
        try {
            Process p = Runtime.getRuntime().exec("cat /proc/self/mounts");
            InputStream is = null;
            //if (p.waitFor() == 0) {
                is = p.getInputStream();
            /*} else {
                is = p.getErrorStream();
            }*/
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String tmp;
            SortedSet set = new TreeSet();

            while ((tmp = br.readLine()) != null)
            {
                set.add(tmp);
            }
            is.close();
            br.close();

            Iterator it = set.iterator();
            s3=it.next().toString().split(" ",3);
            arch.put(s3[1],s3[2]);
            tmp=s3[0];
            while (it.hasNext()) {
                while (it.hasNext()) {
                    s3=it.next().toString().split(" ",3);
                    if (!tmp.equals(s3[0])){
                        archT.put("\u25A0"+tmp,arch);
                        arch = new JSONObject();
                        arch.put(s3[1],s3[2]);
                        tmp=s3[0];
                        break;
                    }
                    arch.put(s3[1],s3[2]);
                }
            }

        } catch (Exception ex) {
            arch.put("error", errorS(ex));// + '\n' + Log.getStackTraceString(ex));
        }
        return archT;
    }

    private JSONObject dumpsysL() throws JSONException {
        JSONObject archT = new JSONObject();
        String s = getZinfo("dumpsys -l","/",true);
        archT.put("dumpsys-l",s.replaceAll("\\."," "));
        s= getZpack("getprop",".svc.",false);
        s=s.replaceAll("init\\u002Esvc\\u002E","");
        s=s.replaceAll("\\]: \\[running\\]","");
        s=s.replaceAll("\\]: \\[stopped\\]","\u25A0");
        s=s.replaceAll("\\[","");
        return archT.put("InitSVC",s.replaceAll("\\."," "));
    }

    private JSONObject systemFL() throws JSONException {
        JSONObject archT = new JSONObject();
        SortedSet set = new TreeSet();
        String s = "" ;
        PackageManager packageManager = mContext.getPackageManager();
        FeatureInfo[] featureInfos = packageManager.getSystemAvailableFeatures();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] Libs = packageManager.getSystemSharedLibraryNames();
            Arrays.sort(Libs);
            for (String ss : Libs) s+= " / "+ ss.replaceAll("\\."," ");
            archT.put("SystemSharedLibraryNames", s);
            s="";
        }
        for (FeatureInfo featureInfo : featureInfos) {
            if (featureInfo != null && featureInfo.name != null ) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    if (featureInfo.version !=0){
                        set.add(featureInfo.name+"\u25A0"+(String.valueOf(featureInfo.version)));
                    } else set.add(featureInfo.name+" °");
                }
                else set.add(featureInfo.name);
            }
        }
        Iterator it = set.iterator();
        while (it.hasNext()) {
            s+="/ "+it.next().toString();
        }
        return archT.put("Features",s.replaceAll("\\."," "));
    }


    private JSONObject systemArch() throws JSONException {
        JSONObject archT = new JSONObject();
        JSONObject arch = new JSONObject();
        archT.put("uname-m",getZinfo("uname -m","",false));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            arch.put("SUPPORTED_ABIS", new JSONArray(Build.SUPPORTED_ABIS));
            arch.put("SUPPORTED_32_BIT_ABIS", new JSONArray(Build.SUPPORTED_32_BIT_ABIS));
            arch.put("SUPPORTED_64_BIT_ABIS", new JSONArray(Build.SUPPORTED_64_BIT_ABIS));
            try {
                System.loadLibrary("binderdetector");
                arch.put("BinderArmProtocol","v"+get_binder_version());
            } catch (UnsatisfiedLinkError e) {
                arch.put("BinderArmProtocol","null(<" + e + ">)");
            }
        } else {
            arch.put("CPU_ABI", Build.CPU_ABI);
            arch.put("CPU_ABI2", Build.CPU_ABI2);
        }
        return archT.put(System.getProperty("os.arch"),arch);
    }

    private JSONObject meta() throws JSONException {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(tz);
        String nowAsISO = df.format(new Date());
        JSONObject archT = new JSONObject()
                .put("versionName", BuildConfig.VERSION_NAME)
                .put("versionCode", BuildConfig.VERSION_CODE)
                .put("timestamp", nowAsISO);
        try {
            archT.put("targetedSDK",mContext.getPackageManager().getApplicationInfo(BuildConfig.APPLICATION_ID,PackageManager.GET_META_DATA).targetSdkVersion);
        } catch (PackageManager.NameNotFoundException e) {
            archT.put("targetedSDK", errorS(e));
        }
        return archT;
    }

    private JSONObject displayInfo() throws JSONException {
        JSONObject archT = new JSONObject();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            String s;
            DisplayManager dm = (DisplayManager) mContext.getSystemService(Context.DISPLAY_SERVICE);
            for (Display display : dm.getDisplays()) {//(WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay()
                s = display.getName().replace(" ","");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) s += "_"+display.isHdr()+"HDR";
                archT.put(s ,displaySpec(display));
            }
        } else {
            PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
            //noinspection deprecation
            WindowManager wm = (WindowManager) mContext
                    .getSystemService(Context.WINDOW_SERVICE);
            archT.put((wm.getDefaultDisplay().getDisplayId()+"_"+pm.isScreenOn()).replaceAll(" ",""), displaySpec(wm.getDefaultDisplay()));
        }
        return archT;
    }

    private JSONObject displaySpec(Display display) throws JSONException {
        JSONObject marshmalow = new JSONObject();
        String s = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
            marshmalow.put("deviceProductInfo", display.getDeviceProductInfo());
            //marshmalow.put("roundedCorner", display.getRoundedCorner(?));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            marshmalow.put("Name", display.getName());
            /*int f = display.getFlags();
            if ((f & Display.FLAG_SUPPORTS_PROTECTED_BUFFERS) !=0 ) s +="_SUPPORTS_PROTECTED_BUFFERS,";
            if ((f & Display.FLAG_SECURE) !=0 ) s +="_SECURE,";
            if ((f & Display.FLAG_PRIVATE) !=0 ) s +="_PRIVATE,";
            if ((f & Display.FLAG_PRESENTATION) !=0 ) s +="_PRESENTATION,";
            if ((f & Display.FLAG_ROUND) !=0 ) s +="_ROUND,";
            /*if ((f & Display.FLAG_CAN_SHOW_WITH_INSECURE_KEYGUARD) !=0 ) s +="_CAN_SHOW_WITH_INSECURE_KEYGUARD,";
            if ((f & Display.FLAG_SHOULD_SHOW_SYSTEM_DECORATIONS) !=0 ) s +="_SHOULD_SHOW_SYSTEM_DECORATIONS,";
            if ((f & Display.FLAG_SCALING_DISABLED) !=0 ) s +="_SCALING_DISABLED,";
            marshmalow.put("Flags", s);*/
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH){
                String[] z = {"UNKNOWN","OFF","ON","DOZE","DOZE_SUSPEND","VR","ON_SUSPEND"};
                if (display.getState() < z.length) marshmalow.put("State", z[display.getState()]);
                else marshmalow.put("State", display.getState());
            }
        }
        s = display.toString();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getMetrics(displayMetrics);
        int i,ii=0 ;
        marshmalow.put("ro.sf.lcd_density", getProp("ro.sf.lcd_density"));
        marshmalow.put("RefreshRate",display.getRefreshRate());
        i= s.indexOf(", real ");
        if (i>=0) {
            ii= s.indexOf(", DisplayMetrics{");
            if (ii>=0) s=s.substring(0,ii);
            s=s.substring(i+7);
            marshmalow.put("ScreenSpecs",s.substring(0,s.indexOf(",")));
            //marshmalow.put("AllMetrics",s);
            marshmalow.put("ViewingMetrics", displayMetrics.toString());//mContext.getResources().getDisplayMetrics().toString());
        } else {
            //marshmalow.put("AllMetrics",s);//android.view.Display@...
            return marshmalow.put("Metrics", displayMetrics.toString());//mContext.getResources().getDisplayMetrics().toString());
        }
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)marshmalow.put("isHDR",((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().isHdr());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            i= s.indexOf(", hdrCapa");
            ii= s.indexOf("ilities@");
            if (i>=0 && ii>=0) s=s.substring(0,i)+s.substring(ii+16);
            marshmalow.put("AllMetrics",s);
            ///marshmalow.put("getHdrCapabilities" , display.getHdrCapabilities().toString());
            s="";
            String[] zz = {"?not_in_use?","DOLBY_VISION","HDR10","HLG","HDR10plus"};
            Display.HdrCapabilities dHDR =(display.getHdrCapabilities());
            for(int j:dHDR.getSupportedHdrTypes()) {
                if(j<zz.length) s+="+ "+zz[j];
                else s+="+ "+j;
            }
            marshmalow.put("SupportedHdrTypes",s);
            s="";
            Display.Mode[] dModes = (display.getSupportedModes());
            for (Display.Mode mode:dModes)s+=mode.toString();//+","+mode.getRefreshRate()+">"+mode.getPhysicalHeight()+"x"+mode.getPhysicalWidth();
            marshmalow.put("SupportedModes",s);

        } else marshmalow.put("AllMetrics",s);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            marshmalow.put("Mode", display.getMode().toString());
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            s="";
            float[] dRefsreshRates = (display.getSupportedRefreshRates());
            for (float f:dRefsreshRates)s+=f;
            marshmalow.put("RefreshRates21",s);
            /*marshmalow.put("PresentationDeadlineNanos", display.getPresentationDeadlineNanos());
            marshmalow.put("AppVsyncOffsetNanos", display.getAppVsyncOffsetNanos());*/
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (display.getCutout() != null) marshmalow.put("Cutout", display.getCutout().toString());
            else marshmalow.put("Cutout", "null");
            ///marshmalow.put("PreferredWideGamutColorSpace", display.getPreferredWideGamutColorSpace().getName());
            ///30 isMinimalPostProcessingSupported();
        }
        return marshmalow;
    }

    private JSONObject drmInfo() throws JSONException {
        return new JSONObject()
                .put("drm.service.enabled", getProp("drm.service.enabled"))
                .put("modular", modularDrmInfo())
                .put("classic", classicDrmInfo());
                
    }

    private JSONObject classicDrmInfo() throws JSONException {
        JSONObject json = new JSONObject();
        
        DrmManagerClient drmManagerClient = new DrmManagerClient(mContext);
        String[] availableDrmEngines = drmManagerClient.getAvailableDrmEngines();

        JSONArray engines = jsonArray(availableDrmEngines);
        json.put("engines", engines);
        
        try {
            if (drmManagerClient.canHandle("", "video/wvm")) {
                DrmInfoRequest request = new DrmInfoRequest(DrmInfoRequest.TYPE_REGISTRATION_INFO, "video/wvm");
                request.put("WVPortalKey", "OEM");
                DrmInfo response = drmManagerClient.acquireDrmInfo(request);
                String status = (String) response.get("WVDrmInfoRequestStatusKey");
                
                status = new String[]{"HD_SD", null, "SD"}[Integer.parseInt(status)];
                json.put("widevine", 
                        new JSONObject()
                                .put("version", response.get("WVDrmInfoRequestVersionKey"))
                                .put("status", status)
                );
            }
        } catch (Exception e) {
            json.put("error", errorS(e));// + '\n' + Log.getStackTraceString(e));
        }

        drmManagerClient.release();
        
        return json;
    }

    private JSONArray jsonArray(String[] stringArray) {
        JSONArray jsonArray = new JSONArray();
        for (String string : stringArray) {
            if (!TextUtils.isEmpty(string)) {
                jsonArray.put(string);
            }
        }
        return jsonArray;
    }

    private String mediaCodecInfo(MediaCodecInfo mediaCodec) {
        String s = "";
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (mediaCodec.isVendor()) s+="Vendor/";
            if (mediaCodec.isSoftwareOnly()) s+= "Software/";
            if (mediaCodec.isHardwareAccelerated()) s+="Hardware+/";
        }
        String[] types = mediaCodec.getSupportedTypes();
        for (int j = 0; j < types.length; j++) {
            s +="~"+types[j];
        }
        return s;
    }
    
    private JSONObject mediaCodecInfo() throws JSONException {

        ArrayList<MediaCodecInfo> mediaCodecs = new ArrayList<>();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            MediaCodecList mediaCodecList = new MediaCodecList(MediaCodecList.ALL_CODECS);
            MediaCodecInfo[] codecInfos = mediaCodecList.getCodecInfos();
            
            Collections.addAll(mediaCodecs, codecInfos);
        } else {
            for (int i=0, n=MediaCodecList.getCodecCount(); i<n; i++) {
                mediaCodecs.add(MediaCodecList.getCodecInfoAt(i));
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) Collections.sort(mediaCodecs,McComparator);
        JSONObject info = new JSONObject();
        JSONObject jsonDecoders = new JSONObject();
        JSONObject jsonEncoders = new JSONObject();
        for (MediaCodecInfo mediaCodec : mediaCodecs) {
            if (mediaCodec.isEncoder()) {
                jsonEncoders.put(mediaCodec.getName(), mediaCodecInfo(mediaCodec));
            } else {
                jsonDecoders.put(mediaCodec.getName(), mediaCodecInfo(mediaCodec));
            }
        }
        info.put("decoders", jsonDecoders);
        info.put("encoders", jsonEncoders);

        return info;
        
    }

    private static Comparator<MediaCodecInfo> McComparator = new Comparator<MediaCodecInfo>(){
        public int compare(MediaCodecInfo m1,MediaCodecInfo m2){
            String mName1 = m1.getName().toUpperCase();
            String mName2 = m2.getName().toUpperCase();

            //ascending order
            return mName1.compareTo(mName2);
        }
    };

    private JSONObject modularDrmInfo() throws JSONException {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            JSONObject drms = new JSONObject();
            drms.put("widevine", uuidModularDrmInfo(WIDEVINE_UUID));
            drms.put("clearkey", uuidModularDrmInfo(CLEARKEY_UUID));
            drms.put("playready", uuidModularDrmInfo(PLAYREADY_UUID));
            return drms;
        } else {
            return null;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private JSONObject uuidModularDrmInfo(UUID uuid) throws JSONException {
        try {
            if (!MediaDrm.isCryptoSchemeSupported(uuid)) {
                return new JSONObject()
                        .put("isCryptoSchemeSupported", "missing");
            }

            MediaDrm mediaDrm;
            mediaDrm = new MediaDrm(uuid);

            final JSONArray mediaDrmEvents = new JSONArray();

            mediaDrm.setOnEventListener(new MediaDrm.OnEventListener() {
                @Override
                public void onEvent( MediaDrm md, byte[] sessionId, int event, int extra, byte[] data) {
                    try {
                        String encodedData = data == null ? null : Base64.encodeToString(data, Base64.NO_WRAP);

                        mediaDrmEvents.put(new JSONObject().put("event", event).put("extra", extra).put("data", encodedData));
                    } catch (JSONException e) {
                        Log.e(TAG, "JSONError", e);
                    }
                }
            });

            try {
                byte[] session;
                session = mediaDrm.openSession();
                mediaDrm.closeSession(session);
            } catch (Exception e) {
                mediaDrmEvents.put(new JSONObject().put("Exception(openSession)", errorS(e)));
            }

            ArrayList<String> stringProps= new ArrayList<String>();
            String[] byteArrayProps = {MediaDrm.PROPERTY_DEVICE_UNIQUE_ID, "provisioningUniqueId", "serviceCertificate"};
            stringProps.add(MediaDrm.PROPERTY_VENDOR);
            stringProps.add(MediaDrm.PROPERTY_VERSION);
            stringProps.add(MediaDrm.PROPERTY_DESCRIPTION);
            stringProps.add(MediaDrm.PROPERTY_ALGORITHMS);

            if (uuid.equals(WIDEVINE_UUID)) {
                stringProps.add("securityLevel");
                stringProps.add("systemId");
                stringProps.add("privacyMode");
                stringProps.add("sessionSharing");
                stringProps.add("usageReportingSupport");
                stringProps.add("appId");
                stringProps.add("origin");
                stringProps.add("hdcpLevel");
                stringProps.add("maxHdcpLevel");
                stringProps.add("maxNumberOfSessions");
                stringProps.add("numberOfOpenSessions");
            }

            JSONObject props = new JSONObject();

            for (String prop : stringProps) {
                String value;
                try {
                    value = mediaDrm.getPropertyString(prop);
                } catch (Exception e) {
                    value = "<unknown>";
                }
                props.put(prop, value);
            }
        /*for (String prop : byteArrayProps) {
            String value;
            try {
                value = Base64.encodeToString(mediaDrm.getPropertyByteArray(prop), Base64.NO_WRAP);
            } catch (IllegalStateException|NullPointerException e) {
                value = "<unknown>";
            }
            props.put(prop, value);
        }//privateIDs ?*/

            JSONObject response = new JSONObject();
            response.put("properties", props);
            response.put("events", mediaDrmEvents);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) mediaDrm.close();
            mediaDrm.release();
            return response;
        } catch (Exception e) {//UnsupportedSchemeException
            return new JSONObject()
                    .put("MediaDrm", "Unsupported init");
        }
    }

    private JSONObject androidInfo() throws JSONException {
        JSONObject marshmalow = new JSONObject();
        marshmalow.put("RELEASE", Build.VERSION.RELEASE);
        marshmalow.put("KERNEL", System.getProperty("os.version"));
        marshmalow.put("SDK_INT", Build.VERSION.SDK_INT);
        marshmalow.put("TAGS", Build.TAGS);
        marshmalow.put("systemName", System.getProperty("user.name"));
        marshmalow.put("osName", System.getProperty("os.name"));
        marshmalow.put("JAVA", System.getProperty("java.vendor"));
        marshmalow.put("JAVA_url", System.getProperty("java.vendor.url"));
        marshmalow.put("JAVA_vm_version", System.getProperty("java.vm.version"));
        marshmalow.put("JAVA_home", System.getProperty("java.home"));
        marshmalow.put("JAVA_path", System.getProperty("java.class.path"));
        marshmalow.put("JAVA_version", System.getProperty("java.version"));
        marshmalow.put("JAVA_runtime", System.getProperty("java.runtime.version"));
        marshmalow.put("ro.build.flavor", getProp("ro.build.flavor"));
        marshmalow.put("ro.build.characteristics",getProp("ro.build.characteristics"));
        marshmalow.put("ro.crypto.type", getProp("ro.crypto.type"));
        String[] z= {"UNSUPPORTED"
                ,"INACTIVE"
                ,"ACTIVATING"
                ,"ACTIVE"
                ,"ACTIVE_DEFAULT_KEY"
                ,"ACTIVE_PER_USER"
        };
        if (((DevicePolicyManager) mContext.getSystemService(Context.DEVICE_POLICY_SERVICE)).getStorageEncryptionStatus() < z.length)
            marshmalow.put("dpmStorageEncryption", z[((DevicePolicyManager) mContext.getSystemService(Context.DEVICE_POLICY_SERVICE)).getStorageEncryptionStatus()]);
        else marshmalow.put("dpmStorageEncryption", ((DevicePolicyManager) mContext.getSystemService(Context.DEVICE_POLICY_SERVICE)).getStorageEncryptionStatus());
/*
        DevicePolicyManager dpm = (DevicePolicyManager) mContext.getSystemService(Context.DEVICE_POLICY_SERVICE);
        marshmalow.put(dpm.getClass().getName(),blindM(dpm));
*/

        marshmalow.put("ALLOWED_GEOLOCATION_ORIGINS", Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.ALLOWED_GEOLOCATION_ORIGINS));
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            marshmalow.put("LOCATION_PROVIDERS_ALLOWED", Settings.Secure.getString(mContext.getContentResolver(),
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED));
        } else marshmalow.put("LOCATION_MODE", Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.LOCATION_MODE));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            marshmalow.put("BASE_OS", Build.VERSION.BASE_OS);
            marshmalow.put("PREVIEW_SDK_INT", Build.VERSION.PREVIEW_SDK_INT);
            marshmalow.put("SECURITY_PATCH", Build.VERSION.SECURITY_PATCH);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) marshmalow.put("RELEASE_OR_CODENAME", Build.VERSION.RELEASE_OR_CODENAME);
            marshmalow.put("ro.secure", getProp("ro.secure"));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                marshmalow.put("TrebleGetprop", getProp("ro.treble.enabled"));
                marshmalow.put("ABupdateGetprop", getProp("ro.build.ab_update"));
                marshmalow.put("ro.postinstall.fstab.prefix", getProp("ro.postinstall.fstab.prefix"));
                marshmalow.put("sys.isolated_storage_snapshot",getProp("sys.isolated_storage_snapshot"));
                marshmalow.put("ro.build.system_root_image",getProp("ro.build.system_root_image"));
                marshmalow.put("ro.apex.updatable", getProp("ro.apex.updatable"));
                marshmalow.put("ro.control_privapp_permissions", getProp("ro.control_privapp_permissions"));
            }
            marshmalow.put("ro.zygote",getProp("ro.zygote"));
            File vManifest = new File("/vendor/etc/vintf/manifest.xml");
            String s ="vintf";
            if (!vManifest.isFile()) {
                vManifest = new File("/vendor/manifest.xml");
                s="legacy";
            }
            if (vManifest.isFile()) {
                try {
                    InputStream is = new FileInputStream(vManifest);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    String line = null;
                    while ((line = reader.readLine()) != null && !line.contains("sepolicy")) {
                    }
                    if ((line = reader.readLine()) != null) {
                        line = line.substring(line.indexOf(">"));
                        marshmalow.put("VendorSepolicy", s + ": " + line.substring(1,line.indexOf("<")));
                    }
                    is.close();
                } catch (Exception e) {

                }
            }
            marshmalow.put("ro.product.first_api_level", getProp("ro.product.first_api_level"));
        }
        marshmalow.put("/Vendor", getZpack("df vendor","vendor",true));
        marshmalow.put("/System", getZpack("df system","system",true));
        return marshmalow;
    }

    private JSONObject systemInfo() throws JSONException {
        JSONObject marshmalow = new JSONObject();
        JSONObject archT = new JSONObject();
        marshmalow.put("Cpu", getCpu());
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            marshmalow.put("socMANUFACTURER", getProp(Build.SOC_MANUFACTURER));
            marshmalow.put("socMODEL", getProp(Build.SOC_MODEL));
            marshmalow.put("mediaPerformanceClass", Build.VERSION.MEDIA_PERFORMANCE_CLASS);
            //PRIVATE? marshmalow.put("sku", getProp(Build.SKU));
        }
        marshmalow.put("BOARD", getProp("ro.board.platform"));
        marshmalow.put("HARDWARE", Build.HARDWARE);
        marshmalow.put("nbrCamsAPI1", numCameras);
        marshmalow.put("CamVendorHAL3", getProp("persist.vendor.camera.HAL3.enabled"));
        marshmalow.put("CamVendorAux", getProp("vendor.camera.aux.packagelist"));
        marshmalow.put("CamAux", getProp("camera.aux.packagelist"));
        marshmalow.put("KernelFull", getZinfo("uname -a","",false));
        marshmalow.put("CODENAME", Build.VERSION.CODENAME);
        marshmalow.put("BOOTLOADER", Build.BOOTLOADER);
        marshmalow.put("BRAND", Build.BRAND);
        marshmalow.put("MODEL", Build.MODEL);
        marshmalow.put("MANUFACTURER", Build.MANUFACTURER);
        marshmalow.put("DEVICE", Build.DEVICE);
        marshmalow.put("ID", Build.ID);
        marshmalow.put("HOST", Build.HOST);
        marshmalow.put("PRODUCT", Build.PRODUCT);
        marshmalow.put("TYPE", Build.TYPE);
        marshmalow.put("USER", Build.USER);
        marshmalow.put("DISPLAY", Build.DISPLAY);
        marshmalow.put("INCREMENTAL", Build.VERSION.INCREMENTAL);
        marshmalow.put("RadioVersion", Build.getRadioVersion());
        marshmalow.put("FINGERPRINT", Build.FINGERPRINT);
        marshmalow.put("FINGERPRINTvendor", getProp("ro.vendor.build.fingerprint"));
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            archT.put("avb_version", getProp("ro.boot.avb_version"));
            archT.put("bootdevice", getProp("ro.boot.bootdevice"));
            archT.put("dynamic_partitions", getProp("ro.boot.dynamic_partitions"));
            archT.put("dynamic_partitions_retrofit", getProp("ro.boot.dynamic_partitions_retrofit"));
            archT.put("super_partition", getProp("ro.boot.super_partition"));
            archT.put("slot_suffix", getProp("ro.boot.slot_suffix"));
            archT.put("flash.locked", getProp("ro.boot.flash.locked"));
            archT.put("product.hardware.sku", getProp("ro.boot.product.hardware.sku"));
            archT.put("vbmeta.size", getProp("ro.boot.vbmeta.size"));
            archT.put("vbmeta.hash_alg", getProp("ro.boot.vbmeta.hash_alg"));
            archT.put("verifiedbootstate", getProp("ro.boot.verifiedbootstate"));
            archT.put("veritymode", getProp("ro.boot.veritymode"));
            marshmalow.put("ro.boot", archT);
        }

        final ActivityManager activityManager =
                (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        final ConfigurationInfo configurationInfo =
                activityManager.getDeviceConfigurationInfo();
        marshmalow.put("glEsVersion", configurationInfo.getGlEsVersion());
        marshmalow.put("reqGlEsVersion", String.valueOf(configurationInfo.reqGlEsVersion));

        //marshmalow.put("zz", mContext.getPackageManager().hasSystemFeature (PackageManager.FEATURE_OPENGLES_EXTENSION_PACK));
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            JSONObject arch = new JSONObject();
            for (Build.Partition bp : Build.getFingerprintedPartitions()) {
                arch.put(bp.getName(), bp.getFingerprint());
            }
            marshmalow.put("FingerprintedPartitions_" + Build.Partition.PARTITION_NAME_SYSTEM, arch);
        }

        return marshmalow;
    }

    private JSONObject securityProviders() throws JSONException {
        Provider[] providers = Security.getProviders();
        JSONObject providergroup = new JSONObject();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) Arrays.sort(providers, new Comparator<Provider>() {
            @Override
            public int compare(final Provider o1, final Provider o2) {
                return (o1.getName().compareTo(o2.getName()));
            }
        });
        for (int i = 0; i < providers.length; i++) {
            JSONObject provider = new JSONObject().put("Info",providers[i].getInfo());

            provider.put("Version",String.valueOf(providers[i].getVersion()));
            provider.put("Class",providers[i].getClass().getName());
            providergroup.put(providers[i].getName(), provider);

        }
        return providergroup;
    }

    private JSONObject rootInfo() throws JSONException {

        JSONObject info = new JSONObject();

        String[] paths = { "/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su",
                "/system/bin/failsafe/su", "/data/local/su" , "/data/adb/magisk.img" ,"/data/magisk.img" };
        JSONArray files = new JSONArray();
        for (String path : paths) {
            if (new File(path).exists()) {
                files.put(path);
            }
        }
        info.put("existingFiles", files);

        return info;
    }

    // NOTE: this application is meant for user-initiated diagnostics. 
    // It doesn't attempt to use the best security practices or to validate the result. 
//    private JSONObject collectSafetyNet() throws JSONException {
//        GoogleApiClient client = new GoogleApiClient.Builder(mContext)
//                .addApi(SafetyNet.API)
//                .build();
//        ConnectionResult connectionResult = client.blockingConnect(20, TimeUnit.SECONDS);
//        if (!connectionResult.isSuccess()) {
//            return new JSONObject().put("connectionError", connectionResult.toString());
//        }
//        byte[] nonce = getRequestNonce();
//        SafetyNetApi.AttestationResult result = SafetyNet.SafetyNetApi.attest(client, nonce).await(20, TimeUnit.SECONDS);
//                        Status status = result.getStatus();
//        if (!status.isSuccess()) {
//            return new JSONObject().put("testingError", status.toString());
//        }
//        String jwsResult = result.getJwsResult();
//
//        // Extract the payload, ignore the rest.
//        String[] parts = jwsResult.split("\\.");
//        if (parts.length != 3) {
//            return new JSONObject().put("invalidResponse", jwsResult);
//        }
//
//        String decoded = new String(Base64.decode(parts[1], Base64.URL_SAFE));
//
//        JSONObject jsonObject = new JSONObject(decoded);
//
//        // Remove the boring keys
//        for (String key : new String[]{"nonce", "timestampMs", "apkPackageName", "apkCertificateDigestSha256", "apkDigestSha256"}) {
//            jsonObject.remove(key);
//        }
//
//        return jsonObject;
//    }

    private byte[] getRequestNonce() {
        byte[] bytes = new byte[32];
        new Random().nextBytes(bytes);
        return bytes;
    }
    /*private String ipAddress() {
        try {
            for (final Enumeration<NetworkInterface> enumerationNetworkInterface = NetworkInterface.getNetworkInterfaces(); enumerationNetworkInterface.hasMoreElements();) {
                final NetworkInterface networkInterface = enumerationNetworkInterface.nextElement();
                for (Enumeration<InetAddress> enumerationInetAddress = networkInterface.getInetAddresses(); enumerationInetAddress.hasMoreElements();) {
                    final InetAddress inetAddress = enumerationInetAddress.nextElement();
                    if (! inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
            return "";
        }
        catch (final Exception e) {
            return "";
        }
    }*/
    //
    private static String getZpack(String s, String grp, boolean bool) {
        try {
            Process p = Runtime.getRuntime().exec(s);
            InputStream is = null;
            //if (p.waitFor() == 0) {
            is = p.getInputStream();
            /*} else {
                is = p.getErrorStream();
            }*/
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String tmp;
            String tmp2 = "";
            if (bool) tmp = br.readLine();
            while ((tmp = br.readLine()) != null)
            {
                if (tmp.contains(grp)) tmp2 +="\n"+tmp;//.replaceAll(" ","");
            }
            is.close();
            br.close();
/*            if (grp.equals(".svc.")) {
                tmp2=tmp2.replaceAll("init\\u002Esvc\\u002E","");
                tmp2=tmp2.replaceAll("\\]: \\[running\\]","");
                tmp2=tmp2.replaceAll("\\]: \\[stopped\\]","\u25A0");
                tmp2=tmp2.replaceAll("\\[","");
            }*/
            if (tmp2.length() != 0) return tmp2.replaceAll("\\n"," /");
            return "Unknow";
        } catch (Exception ex) {
            return errorS(ex);
        }
    }

    private static String getCpu() {
        try {
            Process p = Runtime.getRuntime().exec("cat /proc/cpuinfo");
            InputStream is = null;
            //if (p.waitFor() == 0) {
            is = p.getInputStream();
            /*} else {
                is = p.getErrorStream();
            }*/
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String tmp;
            String tmp2 = null;

            while ((tmp = br.readLine()) != null)
            {
                if (tmp.contains("Hardware")) return tmp.substring(11);
                if (tmp.contains("model name")) tmp2= tmp.substring(13);
            }
            is.close();
            br.close();
            if (tmp2 != null) return tmp2;
            return "Unknow";
        } catch (Exception ex) {
            return errorS(ex);
        }
    }

}

